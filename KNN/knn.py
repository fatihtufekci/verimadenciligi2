import xlrd


wbb = xlrd.open_workbook("/home/fatih/Desktop/veriSon.xls")
sheet = wbb.sheet_by_index(0)

def sinifListesi(k):
    listBasarisiz = []
    listBasarili = []
    listOlgun = []

    counter = 0
    for i in range(sheet.nrows - 1):
        a = sheet.cell_value(i + 1, 4)
        if a == 1:
            listBasarisiz.append(counter)
        elif a == 2:
            listBasarili.append(counter)
        else:
            listOlgun.append(counter)
        counter += 1

    return listBasarisiz, listBasarili, listOlgun


actorBasarisizList,actorBasariliList,actorOlgunList = sinifListesi(6)
turBasarisizList, turBasariliList, turOlgunList = sinifListesi(7)

actorList = []
actorList.append(actorBasarisizList)
actorList.append(actorBasariliList)
actorList.append(actorOlgunList)

turList = []
turList.append(turBasarisizList)
turList.append(turBasariliList)
turList.append(turOlgunList)


def manhattanUzakligi(testActor, testTur):
    actorSonuc = []
    turSonuc = []
    for i in range(sheet.nrows - 1):
        actor = sheet.cell_value(i + 1, 6)
        tur = sheet.cell_value(i+1, 7)

        actorSonuc.append(int(abs(testActor-actor)))
        turSonuc.append(int(abs(testTur-tur)))

    return actorSonuc, turSonuc


from KNN.VeriCekme import AktorTur

def sayiHalineGetir(diziAktorleri, diziTuru):
    a = AktorTur()
    aktorListesi = a.actorDonder()
    turListesi = a.turDonder()
    puanAktor = 0
    puanTur = 0
    try:
        for i in range(len(diziAktorleri)):
            if diziAktorleri[i] in aktorListesi:
                puanAktor += aktorListesi[diziAktorleri[i]]
        for j in range(len(diziTuru)):
            if diziTuru[j] in turListesi:
                puanTur += turListesi[diziTuru[j]]
    except:
        print("Bu Aktor listede yok")

    return puanAktor, puanTur




# ACTOR PUANI VE TUR PUANI VER
diziAktorleri = ["Kenan Imirzalioglu","Çetin Tekindor","Gürkan Uygun","Eslem Akar"]
diziTuru = ["Comedy", "Action"]

#diziAktorleri = ["Sina Ozer"]
#diziTuru = ["War"]

#diziAktorleri = ["Büsra Develi", "Çetin Tekindor"]
#diziTuru = ["War", "Romance"]

print("")
print("")
print("**************************************************")

print("Girdiğimiz dizi aktörleri: ", diziAktorleri)
print("Girdiğimiz dizi türleri: ", diziTuru)

actorPuan, turPuan = sayiHalineGetir(diziAktorleri, diziTuru)

actorSonuc, turSonuc = manhattanUzakligi(actorPuan, turPuan)

def indexAl(sonucDizisi, k):
    actorIndex = []
    for z in range(k):
        actorIndex.append(0)

    for j in range(k):
        minActor = sonucDizisi[0]
        for i in range(len(sonucDizisi) - 1):
            if minActor > sonucDizisi[i + 1]:
                minActor = sonucDizisi[i + 1]
                actorIndex[j] = i + 1
        sonucDizisi.pop(actorIndex[j])

    for x in range(k):
        actorIndex[x] = actorIndex[x] + x

    return actorIndex



# KNN de k değerimiz
actorIndex = indexAl(actorSonuc,3)
turIndex = indexAl(turSonuc,3)


def knnUygula(index, list):
    basarili = 0
    basarisiz = 0
    olgun = 0
    for t in range(len(index)):
        if index[t] in list[0]:
            basarisiz += 1
        elif index[t] in list[1]:
            basarili += 1
        elif index[t] in list[2]:
            olgun += 1
    return basarisiz, basarili, olgun


basarisiz1, basarili1, olgun1 = knnUygula(actorIndex, actorList)
basarisiz2, basarili2, olgun2 = knnUygula(turIndex, turList)

basarisiz = basarisiz1 + basarisiz2
basarili = basarili1 + basarili2
olgun = olgun1 + olgun2

print(basarisiz, basarili, olgun)

minEleman = basarisiz
resultsClassifier = "BAŞARISIZ, dizi ilk sezonunda biter"
if minEleman < basarili:
    minEleman = basarili
    resultsClassifier = "BAŞARILI, dizi 2 yıl sürer"
    if minEleman < olgun:
        minEleman = olgun
        resultsClassifier = "OLGUN, dizi 3+ yıl sürer"
if minEleman < olgun:
    minEleman = olgun
    resultsClassifier = "OLGUN, dizi 3+ yıl sürer"
    if minEleman < basarili:
        minEleman = basarili

print(resultsClassifier)
