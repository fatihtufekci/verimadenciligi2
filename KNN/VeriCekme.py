from encodings.base64_codec import base64_decode
from bs4 import BeautifulSoup
import requests

import xlwt

workbook = xlwt.Workbook()

sheet = workbook.add_sheet("Sheet Name")

# Applying multiple styles
style = xlwt.easyxf('font: bold 1, color red;')

# Writting on specified sheet
sheet.write(0, 0, 'DIZI ADI', style)
sheet.write(0, 1, 'SEZON YILI', style)
sheet.write(0, 2, 'AKTORLER', style)
sheet.write(0, 3, 'BOLUM', style)
sheet.write(0, 4, 'SEZON', style)
sheet.write(0, 5, 'TUR', style)
sheet.write(0, 6, 'AKTOR PUANI', style)
sheet.write(0, 7, 'TUR PUANI', style)
sheet.write(0, 8, 'SEZON DURUMU', style)

counter = 0
actor = []
kopru = []
diziAdi = []
sezonYili = []
tur = []

konu = []
bridge = []

yildizOyuncu = {}

turPuaniBuffer = {}

turDizisi2 = []

counter = 0

#1301
for urlDeger in range(51,151,50):
    newUrl = 'https://www.imdb.com/search/title?title_type=tv_series&countries=tr&start=' + str(urlDeger) + '&ref_=adv_nxt'
    reps = requests.get(newUrl)
    soup = BeautifulSoup(reps.content, 'html.parser')
    ustSayfaVerileri = soup.find_all('div', {"class": "lister-item-content"})

    for i in ustSayfaVerileri:
        if i.find('p').find('span', {"class": "genre"}) == None:
            continue

        diziAdi.append(i.find('h3').find('a').text)

        sheet.write(counter + 1, 0, diziAdi[counter])

        sezonYili.append(i.find('h3').find('span', {"class": "lister-item-year text-muted unbold"}).text)
        tur.append(i.find('p').find('span', {"class": "genre"}).text)
        tur[counter] = tur[counter].translate({ord(i): None for i in ' \n'})
        a = tur[counter]
        sheet.write(counter + 1, 5, a)

        s = sezonYili[counter].translate({ord(i): None for i in ' ()'})
        s1 = s.find('–')

        c = 0
        if s1 == -1:
            c = 1
            sheet.write(counter + 1, 1, s)
            sheet.write(counter + 1, 4, c)
            sheet.write(counter + 1, 8, "BASARISIZ")
        else:
            s2 = s[0:4]
            s3 = s[5:len(sezonYili[counter]) - 1]
            if s3 == '':
                c = 2019 - int(s2)
                d = s2 + "-2019"
                sheet.write(counter + 1, 1, d)
                sheet.write(counter + 1, 4, c)
                if c>=3:
                    sheet.write(counter + 1, 8, "OLGUN")
                elif c==2:
                    sheet.write(counter + 1, 8, "BASARILI")
                elif c < 2:
                    sheet.write(counter + 1, 8, "BASARISIZ")
            else:
                try:
                    c = int(s3) - int(s2)
                    sheet.write(counter + 1, 1, s)
                    sheet.write(counter + 1, 4, c)
                    if c >= 3:
                        sheet.write(counter + 1, 8, "OLGUN")
                    elif c == 2:
                        sheet.write(counter + 1, 8, "BASARILI")
                    elif c < 2:
                        sheet.write(counter + 1, 8, "BASARISIZ")
                except:
                    print("")

        star = i.find_all('p')
        stars = []
        stars = star[2].find_all("a")

        for j in stars:
            kopru.append(j.text)


        actor.append(kopru)

        aktorlerr = ""

        if len(stars) > 0:
            kk = actor[counter][0]
            for q in actor[counter][1:]:
                aktorlerr = kk + "," + str(q)

        kopru = []

        liste = []
        for f in range(len(actor)):
            for j in range(len(actor[f])):
                liste.append(actor[f][j])

        if tur[counter] not in turPuaniBuffer:
            turPuaniBuffer[tur[counter]] = 0

        turDizisi = tur[counter].split(',')
        turDizisi2.append(turDizisi)

        listeTurr = []
        for g in range(len(turDizisi2)):
            for j in range(len(turDizisi2[g])):
                listeTurr.append(turDizisi2[g][j])

        bolum = []
        f = 1
        for p in range(len(turDizisi2[counter])):
            if turDizisi2[counter][p] not in turPuaniBuffer:
                turPuaniBuffer[turDizisi2[counter][p]] = 0

        listTur = []
        for y in turPuaniBuffer:
            listTur.append(listeTurr.count(y))

        for z in range(len(actor[counter])):
            if actor[counter][z] not in yildizOyuncu:
                yildizOyuncu[actor[counter][z]] = 0

        listOyuncu= []
        for x in yildizOyuncu:
            listOyuncu.append(liste.count(x))

        if c == 1:
            # Başarısız
            for z in range(len(actor[counter])):
                if actor[counter][z] in yildizOyuncu:
                    yildizOyuncu[actor[counter][z]] += 5
            # if x in range(len(tur[counter])):
            for k in range(len(turDizisi2[counter])):
                if turDizisi2[counter][k] in turPuaniBuffer:
                    turPuaniBuffer[turDizisi2[counter][k]] += 5
        elif c == 2:
            # Başarılı
            for z in range(len(actor[counter])):
                if actor[counter][z] in yildizOyuncu:
                    yildizOyuncu[actor[counter][z]] += 10

            for k in range(len(turDizisi2[counter])):
                if turDizisi2[counter][k] in turPuaniBuffer:
                    turPuaniBuffer[turDizisi2[counter][k]] += 10
        else:
            # Olgun
            for z in range(len(actor[counter])):
                if actor[counter][z] in yildizOyuncu:
                    yildizOyuncu[actor[counter][z]] += 15

            for k in range(len(turDizisi2[counter])):
                if turDizisi2[counter][k] in turPuaniBuffer:
                    turPuaniBuffer[turDizisi2[counter][k]] += 15

        sheet.write(counter + 1, 2, aktorlerr)

        counter = counter + 1

sayacc = 0
for t in yildizOyuncu:
    if listOyuncu[sayacc]==0:
        yildizOyuncu[t] = 0
    else:
        yildizOyuncu[t] = int(yildizOyuncu[t] / listOyuncu[sayacc])
    sayacc = sayacc + 1

say = 0
for h in turPuaniBuffer:
    if listTur[say] == 0:
        turPuaniBuffer[h] = 0
    else:
        turPuaniBuffer[h] = int(turPuaniBuffer[h] / listTur[say])
    say = say + 1

for j in range(counter):
    puanAktor = 0
    puanTur = 0
    for z in range(len(actor[j])):
        if actor[j][z] in yildizOyuncu:
            puanAktor += yildizOyuncu[actor[j][z]]

    for k in range(len(turDizisi2[j])):
        if turDizisi2[j][k] in turPuaniBuffer:
            puanTur += turPuaniBuffer[turDizisi2[j][k]]

    if len(turDizisi2[j]) == 0:
        sheet.write(j + 1, 7, 0)
    else:
        sheet.write(j + 1, 7, int(puanTur / len(turDizisi2[j])))
    if len(actor[j]) == 0:
        sheet.write(j + 1, 6, 0)
    else:
        sheet.write(j + 1, 6, int(puanAktor / len(actor[j])))

workbook.save("/home/fatih/Desktop/veriM.xls")
#print(len(actor))

print(turPuaniBuffer)
print(yildizOyuncu)

class AktorTur:
    def turDonder(self):
        return turPuaniBuffer
    def actorDonder(self):
        return yildizOyuncu