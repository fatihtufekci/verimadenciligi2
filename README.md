> KNN ile yeni çıkacak dizinin kaç sezon devam edeceğinin tahmini

**Projenin Amacı:** IMDB sitesinden dizi verileri çekilip burada çeşitli veri madenciliği teknikleri kullanıp yeni çıkacak dizinin kaç sezon devam edebileceği verisini tahmin etmektir. 

**Proje Detayı:** IMDB sitesinden dizinin adı, bölüm sayısı, başrol oyuncuları gibi veriler çekilip veri madenciliği uygulamaları ile veri üzerinde temizleme yapılmıştır. Veri Temizlemesinden sonra KNN algoritması kodlanmış ve dizinin belirlenen parametrelere göre kaç bölüm devam edeceği tahmin ettirilmiştir.