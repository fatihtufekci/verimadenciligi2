import xlrd
import pandas

wbb = xlrd.open_workbook("/home/fatih/Desktop/veriSS.xls")
sheet = wbb.sheet_by_index(0)

diziAdi = []
sezonYili = []
aktorler = []
sezon = []
tur = []
actorPuan = []
turPuan = []
sezonDurumu = []

for i in range(sheet.nrows - 1):
    a = sheet.cell_value(i + 1, 0)
    diziAdi.append(sheet.cell_value(i + 1, 0))
    sezonYili.append(sheet.cell_value(i + 1, 1))
    aktorler.append(sheet.cell_value(i + 1, 2))
    sezon.append(sheet.cell_value(i + 1, 3))
    tur.append(sheet.cell_value(i + 1, 4))
    actorPuan.append(sheet.cell_value(i + 1, 5))
    turPuan.append(sheet.cell_value(i + 1, 6))
    sezonDurumu.append(sheet.cell_value(i + 1, 7))

data = {
    "diziAdi": diziAdi, "sezonYili":sezonYili, "aktorler":aktorler, "sezon":sezon,
    "tur":tur, "actorPuan":actorPuan, "turPuan":turPuan, "sezonDurumu":sezonDurumu
}


dataFrame = pandas.DataFrame(data, columns=['diziAdi','sezonYili','aktorler','sezon','tur','actorPuan','turPuan','sezonDurumu'])
dataFrame.to_csv("fatih.csv")