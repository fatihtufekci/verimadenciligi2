import pandas

from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix

data = pandas.read_csv("fatih.csv")

tur = data.iloc[:, 5:6].values
turPuan = data.iloc[:, 7:8].values
sezonDurumu = data.iloc[:, 8:9].values

sc = StandardScaler()
le = LabelEncoder()

turNum = le.fit_transform(tur[:,0])

sezonDurumuDataFrame = pandas.DataFrame(data=sezonDurumu, index=range(100), columns=["sezonDurumu"])
turDataFrame = pandas.DataFrame(data=turNum, index=range(100), columns=["tur"])
turPuanDataFrame = pandas.DataFrame(data=turPuan, index=range(100), columns=["turPuan"])

bData = pandas.concat([turDataFrame, turPuanDataFrame], axis=1)

x_train, x_test, y_train, y_test = train_test_split(bData, sezonDurumuDataFrame, test_size=0.33, random_state=0)

X_train = sc.fit_transform(x_train)
X_test = sc.transform(x_test)

knn = KNeighborsClassifier(n_neighbors=5, metric="minkowski")

knn.fit(X_train, y_train)

y_pred = knn.predict(X_test)

print(type(y_pred))

cs = confusion_matrix(y_test,y_pred)
print(cs)
