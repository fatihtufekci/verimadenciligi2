import xlrd


wbb = xlrd.open_workbook("/home/fatih/Desktop/veriSon.xls")
sheet = wbb.sheet_by_index(0)
print(sheet.nrows)


def ortalamaHesapla(k):
    toplamBasarisiz = 0
    toplamBasarili = 0
    toplamOlgun = 0
    countBasarisiz = 0
    countBasarili = 0
    countOlgun = 0
    for i in range(sheet.nrows - 1):
        a = sheet.cell_value(i + 1, 4)
        if a == 1:
            toplamBasarisiz += int(sheet.cell_value(i + 1, k))
            countBasarisiz += 1
        elif a == 2:
            toplamBasarili += int(sheet.cell_value(i + 1, k))
            countBasarili += 1
        else:
            toplamOlgun += int(sheet.cell_value(i + 1, k))
            countOlgun += 1

    basarisizOrt = int(toplamBasarisiz / countBasarisiz)
    basariliOrt = int(toplamBasarili / countBasarili)
    olgunOrt = int(toplamOlgun / countOlgun)

    return basarisizOrt, basariliOrt, olgunOrt


actorBasarisizOrt,actorBasariliOrt,actorOlgunOrt = ortalamaHesapla(6)
turBasarisizOrt, turBasariliOrt, turOlgunOrt = ortalamaHesapla(7)

print("ACTOR: ",actorBasarisizOrt,actorBasariliOrt,actorOlgunOrt )
print("TUR: ",turBasarisizOrt,turBasariliOrt,turOlgunOrt)


def oklidUzakligiUygula(testActor, testTur):
    print("")
    basarisizDistance = (((testActor-actorBasarisizOrt)**2) + ((testTur - turBasarisizOrt)**2))**0.5

    basariliDistance = (((testActor - actorBasariliOrt) ** 2) + ((testTur - turBasariliOrt) ** 2)) ** 0.5

    olgunDistance = (((testActor - actorOlgunOrt) ** 2) + ((testTur - turOlgunOrt) ** 2)) ** 0.5

    min = basarisizDistance
    sonuc = "Başarısız"
    if min > basariliDistance:
        min = basariliDistance
        if min > olgunDistance:
            min = olgunDistance
            sonuc = "Olgun"
            return sonuc
        sonuc = "Başarılı"
        return sonuc

    print(min)
    return sonuc

result = oklidUzakligiUygula(2, 2)

print(result)